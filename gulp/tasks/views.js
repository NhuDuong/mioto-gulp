const browserSync    = require('browser-sync');
const cached         = require('gulp-cached');
const config         = require('../config');
const filter         = require('gulp-filter');
const gulp           = require('gulp');
const handleErrors   = require('../helpers/handle-errors');
const htmlhint       = require("gulp-htmlhint");
const htmlmin        = require('gulp-htmlmin');
const plumber        = require('gulp-plumber');
const pug            = require('gulp-pug');
const pugInheritance = require('gulp-pug-inheritance');
const viewPath       = config.paths.views;

gulp.task('views:dev', () => {
  return gulp.src(viewPath.all)
    .pipe(plumber({ errorHandler: handleErrors }))
    .pipe(cached('pug'))
    .pipe(pugInheritance({ basedir: 'app/views', skip: 'node_modules' }))
    .pipe(filter(function (file) {
      return !/\/_/.test(file.path) && !/^_/.test(file.relative);
    }))
    .pipe(pug({pretty: true}))
    .pipe(htmlhint({ htmlhintrc: 'gulp/lints/.htmlhintrc' }))
    .pipe(htmlhint.reporter())
    .pipe(gulp.dest(viewPath.dest))
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task('views:deploy', () => {
  return gulp.src(viewPath.src)
    .pipe(plumber({ errorHandler: handleErrors }))
    .pipe(pug({ pretty: true }))
    .pipe(htmlmin({ collapseWhitespace: true, removeComments: true }))
    .pipe(gulp.dest(viewPath.dest));
});