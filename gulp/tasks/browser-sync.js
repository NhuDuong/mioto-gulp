const browserSync = require('browser-sync');
const config = require('../config');
const gulp = require('gulp');

gulp.task('browser-sync:dev', () => {
    browserSync.init({
        server: {
            baseDir: config.paths.dest
        },
        options: {
            reloadDelay: 1000
        },
        open: true,
        notify: true,
    });
});